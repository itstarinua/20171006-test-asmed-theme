    <footer>
        <div class="dms-container">
            <?php \wp_nav_menu( array(
                'container'       => 'nav', 
                'container_class' => 'menu-container', 
                'container_id'    => 'footer-menu-container',
                'echo'            => true,
                'menu_class'      => 'footer-menu',
                'theme_location'  => 'footer_menu',
                'depth'           => 1,
                'fallback_cb'     => '__return_empty_string',
            ) ); ?>  
            <div class="copyright-container">
                <p class="copyright"><?php echo sprintf( ( ( date('Y') == '2016') ? __('All rights reserved %s') : __('All rights reserved 2016 - %s') ), date('Y') );?></p>
                <p class="developer"><?php echo sprintf( __('Website is developed by <a href="%1$s" title="%2$s">%2$s</a>' ), 'https://plus.google.com/u/0/110295925295050770002/posts','DStaroselskyi');?></p>
            </div>
                      
        </div>
    </footer>
</div>
<?php wp_footer(); ?>

</body>
</html>
