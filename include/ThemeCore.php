<?php

namespace DStaroselskiy\Wordpress\Themes;

if( !class_exists( '\DStaroselskiy\Wordpress\Themes\ThemeController') )
{
    require_once 'ThemeController.php';
}

class IntensiveRussian extends ThemeController {
    protected $theme_name = 'intensive-russian';
    protected $menus = array(
        'top_menu' => 'Top menu',
        'footer_menu'  => 'Footer menu',
    ); 
    protected $post_formats = array();
//    protected $sidebars = array();
    
    public static function custom_logo( $blog_id = 0, $path = '', $scheme = null, $echo = false ) {
	$str = '';
        if ( function_exists( 'has_custom_logo' ) && \has_custom_logo( $blog_id ) ) {
            $logo_type = \get_custom_logo( $blog_id );
            $home_url = \get_home_url( ( $blog_id> 0 ? $blog_id : null ), $path, $scheme );
            $site_tytle = \get_bloginfo( 'name' );
            if( \is_home() || \is_front_page() )
            {
               $str = <<<STR
<span class="logotype-container">
            {$logo_type}
</span>            
STR;
            }
            else
            {
                $str = <<<STR
<a class="logotype-container" href="{$home_url}" title="{$site_tytle}" rel="nofollow">
            {$logo_type}
</a>            
STR;
            }            
            if( $echo ) 
            {
                echo $str;
            }
	}
        return $str;
    }
    public static function blog_title( $blog_id = 0, $path = '', $scheme = null, $echo = false ) {
	$str = '';
        if ( function_exists( 'get_bloginfo' ) )
        {
            $site_tytle = \get_bloginfo( 'name' );
            if( !empty( $site_tytle ) )
            {                
                $home_url = \get_home_url( ( $blog_id> 0 ? $blog_id : null ), $path, $scheme );            
                if( \is_home() || \is_front_page() )
                {
                    $str = <<<STR
<h1 class="site-name-containet">
                {$site_tytle}
</h1>            
STR;
                }
                else
                {
                    $str = <<<STR
<a class="site-name-containet" href="{$home_url}" title="{$site_tytle}" rel="nofollow">
                {$site_tytle}
</a>            
STR;
                }
                if( $echo ) 
                {
                    echo $str;
                }
            }
	}
        return $str;
    }
    public static function blog_description( $blog_id = 0, $path = '', $scheme = null, $echo = false ) {
	$str = '';
        if ( \function_exists( 'get_bloginfo' ) )
        {
            $blog_description = \get_bloginfo( 'description' );
            $site_tytle = \get_bloginfo('name');
            if( !empty( $blog_description ) )
            {                
                $home_url = \get_home_url( ( $blog_id> 0 ? $blog_id : null ), $path, $scheme );            
                if( \is_home() || \is_front_page() )
                {
                    $str = <<<STR
<span class="site-description">
                {$blog_description}
</span>            
STR;
                }
                else
                {
                    $str = <<<STR
<a class="site-description" href="{$home_url}" title="{$site_tytle}" rel="nofollow">
                {$blog_description}
</a>            
STR;
                }
                if( $echo ) 
                {
                    echo $str;
                }
            }
	}
        return $str;
    }
    /** function work with hook and init inserting styles to html
     * 
     */

    public function initThemesOptions()
    {
        parent::initThemesOptions();
        \add_theme_support( 'title-tag' );
        \add_theme_support( 'custom-logo', array(
                'height'      => 35,
                'width'       => 220,
                'flex-height' => true,
        ) );
        \add_theme_support( 'custom-header', array(
                'width'         => 1920,
                'height'        => 745,
                'flex-width'    => true,
                'flex-height'    => true,
                'uploads'       => true,
        ));
        \add_theme_support('post-thumbnails');
    }
    //Подключаем файлы стилей
    public function initStyles(){
        parent::initStyles();
       // if( !wp_style_is( 'vc_typicons', 'registered ') ) wp_enqueue_style( 'vc_typicons', DMS_THEME_URL.'/font/typicons.min.css', array(), null ,'all');
    }
    //Подключаем файлы скриптов
    public function initScripts(){
        if( !wp_script_is( 'bootstrap', 'registered ') ) wp_enqueue_script( 'bootstrap', $this->getThemeUrl('/js/bootstrap.min.js'),array('jquery'),null,true);
        if( !wp_script_is( 'jquery-ui-core', 'registered ') ) wp_enqueue_script( 'jquery-ui-core' );
        if( !wp_script_is( 'jquery-ui-selectmenu', 'registered ') ) wp_enqueue_script( 'jquery-ui-selectmenu' );
        if( !wp_script_is( 'jquery-ui-datepicker', 'registered ') ){
            \wp_enqueue_script( 'jquery-ui-datepicker' );
            \wp_localize_jquery_ui_datepicker(); 
        }
//        if( !wp_script_is( 'wow', 'registered ') ) wp_enqueue_script( 'wow', $this->getThemeUrl('/js/wow.min.js'),array('jquery'),null,true);
        //if( !wp_script_is( 'jquery.inputmask.extensions', 'registered ') ) wp_enqueue_script( 'jquery.inputmask.extensions', $this->getThemeUrl('/js/jquery.inputmask.extensions.js'),array('jquery'),null,true);
        //if( !wp_script_is( 'jquery.inputmask.regex.extensions', 'registered ') ) wp_enqueue_script( 'jquery.inputmask.regex.extensions', $this->getThemeUrl('/js/jquery.inputmask.regex.extensions.js'),array('jquery'),null,true);
        parent::initScripts();
    }
    
//    public function page_templates( $template ) {
//	if( is_page('blog')  ){
//            $template_new = $this->getThemeDir( 'templates/pages/page-blog.php') ;
//            if( \file_exists( $template_new ) )
//            {
//               // $template = $template_new;
//            }
//	}
//	return $template;
//    }
//    public function __construct($themeDir = null, $themeUrl = null) {
//        \add_filter( 'template_include', array( &$this, 'page_templates'), 99, 1 );
//        parent::__construct($themeDir, $themeUrl);
//    }
    public static function Run( $themeDir = null, $themeUrl = null )
    {
        if( !is_object( self::$themeCore ) )
        {
            $class_name = __CLASS__;
            self::$themeCore = new $class_name( $themeDir, $themeUrl);
        }
        return self::$themeCore;
    }    
}
