<?php use DStaroselskiy\Wordpress\Themes\ThemeController;
get_header(); ?>
<section id="blog-top-section" class="blog-top-section">
    <img src="<?php echo ThemeController::getUrl('images/icon_blog.jpg');?>" title="<?php \_e('Oops, page not found!', ThemeController::getTextDomain() );?>" alt="<?php \_e('Oops, page not found!', ThemeController::getTextDomain() );?>">
    <h1 class="page-title">404</h1>
    <div class="taxonomy-description"><?php \_e('Oops, page not found!', ThemeController::getTextDomain() );?></div>
</section>
<?php get_footer(); 

?>