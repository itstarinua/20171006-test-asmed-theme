<?php 
/* шаблон вывода любой страницы
 * 
 *  */
use DStaroselskiy\Wordpress\Themes\ThemeController;
get_header(); ?>
    <main class="" role="main">
    <?php if ( have_posts() ) : ?>
 	<?php while ( have_posts() ) : 
            the_post();
            ThemeController::get_template_part( 'content', get_post_format() );
            
            // End the loop.
        endwhile;

        // Previous/next page navigation.
        the_posts_pagination( array(
                'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
                'next_text'          => __( 'Next page', 'twentyfifteen' ),
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
        ) );

    // If no content, include the "No posts found" template.
    else :
        ThemeController::get_template_part( 'content', 'none' );
    endif;
    ?>

    </main><!-- .site-main -->

<?php get_footer();

?>