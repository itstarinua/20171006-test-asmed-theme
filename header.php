<!DOCTYPE html>
<?php use \DStaroselskiy\Wordpress\Themes\IntensiveRussian;?>
<html <?php language_attributes(); ?> class="">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<!--meta name="viewport" content="width=1240px"-->
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="page-container">
        <header>
            <div class="dms-container">
                <?php if( \display_header_text() ) 
                { ?>
                    <div class="site-title">
                        <?php echo IntensiveRussian::custom_logo(); ?>
                        <?php echo IntensiveRussian::blog_title(); ?>
                        <?php echo IntensiveRussian::blog_description(); ?>
                    </div>
                <?php } 
                if( \has_nav_menu( 'top_menu' ) ) 
                { ?>
                    <div class="menu-call-btn-conteiner navbar-header page-scroll pull-right">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php
                        // Primary navigation menu.
                        \wp_nav_menu( array(
                            'container'       => 'nav', 
                            'container_class' => 'menu-container collapse navbar-collapse navbar-ex1-collapse', 
                            'container_id'    => 'top-menu-container',
                            'echo'            => true,
                            'menu_class'     => 'nav-menu',
                            'theme_location' => 'top_menu',
                            'depth'           => 1,
                            'fallback_cb'    => '__return_empty_string',
                        ) );
                } ?>
            </div>
        </header>
