<?php
error_reporting( E_ALL );
ini_set('display_errors', 1);

if( !class_exists('\DStaroselskiy\Wordpress\Themes\IntensiveRussian') ) 
{ 
    require_once 'include/ThemeCore.php'; 
    \DStaroselskiy\Wordpress\Themes\IntensiveRussian::Run();
}

