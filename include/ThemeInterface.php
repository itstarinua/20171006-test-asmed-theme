<?php
/**
 * Description of ThemeInterface
 *
 * @author DStaroselskiy
 * @date 2017-01-22
 */
namespace DStaroselskiy\Wordpress\Themes;

interface ThemeInterface
{
    /* Функция возвращает имя текстового домена 
     * @return string - текстовый домен темы
     */
    public static function getTextDomain();
    /* Функция возвращает название темы и добавляет в конец переданную строку
     * $str string строка добавляемая после имене темы
     * @return string - имя темы
     */
    public function getThemeName( $str = '');
    public function initThemesOptions();
    public function clearWordpressHead();
    public function initStyles();
    public function initScripts();
    public function registerSidebars();
}

?>