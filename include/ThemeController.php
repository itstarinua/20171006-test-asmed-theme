<?php
/**
 * Description of ThemeCore
 *
 * @author DStaroselskiy
 * @date 2017-01-22
 */
namespace DStaroselskiy\Wordpress\Themes;

if( !interface_exists( '\DStaroselskiy\Wordpress\Themes\ThemeInterface') )
{
    require_once 'ThemeInterface.php';
}

class ThemeController implements ThemeInterface
{
    public static $themeCore = null;
    public static $text_domain = 'dms_theme';
    protected $theme_name = __Class__;
    protected $theme_version = '0.0.1';
    protected $menus = array();
    protected $html5 = array('search-form','comment-form','comment-list','gallery','caption',);
    protected $post_formats = array('image','video',);
    protected $themeDir = '';
    protected $themeUrl = '';
    protected $sidebars = array(
        array(
            'name' => 'Right sidebarin blog',
            'id' => "sidebar-right",
            'description' => '',
            'class' => '',
            'before_widget' => '<li id="%1$s" class="widget %2$s">',
            'after_widget' => "</li>\n",
            'before_title' => '<h2 class="widgettitle">',
            'after_title' => "</h2>\n",
        ), 
    );
    /* Функция возвращает название темы и добавляет в конец переданную строку
     * $str string строка добавляемая после имене темы
     * @return string - имя темы
     */
    public function getThemeName( $str = '')
    {
        return $this->theme_name.$str;
    }
    public static function get_template_part( $dir, $name )
    {
       return \get_template_part( 'templates/'.$dir.'/'.$dir, $name ); 
    }
    public static function the_header_name( $before = '', $after = '')
    {
        echo self::get_header_name( $before, $after);
    }
    public static function get_header_name( $before = '', $after = '')
    {
        if( \is_category() )
        {
            return $before . \single_cat_title('', false) . $after;
        }
        elseif( \is_tag() ) 
        {
            return $before .  \single_tag_title( '', false ) . $after;
	} 
        elseif ( is_author() ) 
        {
		return $before .   \get_the_author() . $after;
	}
        elseif( \is_single() || \is_page()) 
        {
            return $before . \get_the_title() . $after;
        }
        elseif( \is_404()) 
        {
            return $before . '404' . $after;
        }
        elseif( \is_home() )
        {
            $id = \get_option( 'page_for_posts',0 );
            $title = \get_the_title( (int)$id );
            if( !empty( $title ) )
            {
                return $before . $title . $after;
            }
        }
        return $before . \get_the_archive_title( ) . $after;
    }
    /* Функция возвращает имя текстового домена 
     * @return string - текстовый домен темы
     */
    public static function getTextDomain()
    {
        if( empty( self::$text_domain ) ) 
        {
            $class_name = end( explode( '\\', __CLASS__) );
            if( $class_name === false ) $class_name = __CLASS__;
            self::$text_domain = $class_name;
        }
        return self::$text_domain;
    }
    protected function setThemeDir( $dir )
    {
        $this->themeDir = $dir;
        return $this;
    }
    protected function setThemeUrl( $url )
    {
        $this->themeUrl = $url;
        return $this;
    }
    public function getThemeDir( $folder = '' )
    {
        return $this->themeDir.( isset( $folder[0] ) ? ( ( $folder[0] == '/' ) ? $folder : '/'.$folder ) : '');
    }
    public function getThemeUrl( $url = '' )
    {
        return $this->themeUrl.( isset( $url[0] ) ? ( ( $url[0] == '/' ) ? $url : '/'.$url ) : '');
    }       
    
    public function scriptForIE9()
    {
        echo <<<STR
        <!--[if lt IE 9]>
            <script src="{$this->getThemeUrl('js/vendor/html5shiv.min.js')}"></script>
            <script src="{$this->getThemeUrl('js/vendor/respond.min.js')}"></script>
            <script src="{$this->getThemeUrl('js/vendor/jquery-1.12.4.min.js')}"></script>
        <![endif]-->    
STR;
    }
    protected function initMenus()
    {   
        if( count( $this->getMenus() ) )
        {
            $menu = array();
            foreach( $this->getMenus() as $key => $name )
            {
               $menu[ $key ] = \__( $name, self::getTextDomain() );
            }
            \register_nav_menus( $menu );
        }
    }    
    public function clearWordpressHead()
    {
        //Очищаем заготовок CMS
        remove_action( 'wp_head', 'feed_links_extra', 3 ); 
        remove_action( 'wp_head', 'feed_links', 2 );
        remove_action( 'wp_head', 'rsd_link' );
        remove_action( 'wp_head', 'index_rel_link' );
        remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); 
        remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
        remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
        remove_action( 'wp_head', 'wp_generator' );
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
        remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
        remove_action( 'wp_head', 'wp_oembed_add_host_js' );
        //Удаляем "манифест". Он подключает специальный XML-файл к сайту,  чтобы включить возможность взаимодействия блога на движке WordPress с приложением Windows Live Writer.
        remove_action('wp_head', 'wlwmanifest_link');
    }
    public function initStyles()
    {
        if( !( \wp_style_is( $this->getThemeName('-core'), 'registered ') ) )
        {
            \wp_enqueue_style( $this->getThemeName('-core'), $this->getThemeUrl('css/'.$this->getThemeName('.css')), array(), null ,'all');
        }
        if( !( \wp_style_is( $this->getThemeName('-style'), 'registered ') ) )
        {
            \wp_enqueue_style( $this->getThemeName('-style'), $this->getThemeUrl('style.css'), array(), null ,'all');
        }
    }
    public function initScripts()
    {
        if( !( \wp_script_is( 'jquery', 'registered ') ) )
        {
            \wp_enqueue_script( 'jquery', $this->getThemeUrl('js/jquery-1.11.3.min.js'),array(),'1.11.3',true);
        }
        if( !( \wp_script_is( $this->getThemeName('-core'), 'registered ') ) )
        {
            \wp_enqueue_script( $this->getThemeName('-core'), $this->getThemeUrl('js/core.js'),array('jquery'),null,true);
        }
    }
    public function initThemesOptions()
    {
        $this->initMenus(); 
        \load_theme_textdomain( $this->getThemeName(), $this->getThemeDir( '/languages' ) );
        \add_theme_support( 'html5', $this->html5 );
        \add_theme_support( 'post-formats', $this->post_formats );
        \add_action( 'widgets_init', array( &$this, 'registerSidebars' ) ); // Регистрируем два сайдбара для вівода виджетов с параметрами:  Один с float:left, другой с float:right 
    }
    public function initScriptsAndStyles(){
        $this->initStyles();
        $this->initScripts();
    }   
    protected function getSidebars()
    {
       return \apply_filters( $this->getThemeName('_getSidebars'), $this->sidebars); 
    }
    protected function getMenus()
    {
       return \apply_filters( $this->getThemeName('_getMenus'), $this->menus); 
    }
    protected function registerThemesSidebars()
    {
        if ( \function_exists( 'register_sidebar' ) 
                && count( $this->getSidebars() ) 
        ) {
            foreach( $this->getSidebars() as $sidebar )
            {
                if( isset( $sidebar['name'] ) && !empty( $sidebar['name'] )
                    && isset( $sidebar['id'] ) && !empty( $sidebar['id'] )
                ) {
                    $sidebar['name'] = \__( $sidebar['name'], self::getTextDomain() );
                    \register_sidebar( $sidebar );
                }
            }
        }
    }
    public function registerSidebars()
    {
        $this->registerThemesSidebars();
    }
    public static function getUrl( $url = '' )
    {
        if( self::$themeCore === null ) return $url;
        return self::$themeCore->getThemeUrl( $url );
    }
    public static function getDir( $dir = '' )
    {
        if( self::$themeCore === null ) return $dir;
        return self::$themeCore->getThemeDir( $dir );
    }
    public function __construct( $themeDir = null, $themeUrl = null ) 
    {
        $this->setThemeDir( empty( $themeDir ) ? \get_template_directory() : $themeDir );
        $this->setThemeUrl( empty( $themeUrl ) ? \get_template_directory_uri() : $themeUrl );
        if( !( \has_action( 'after_setup_theme', array( &$this, 'after_setup_theme' ) ) ) )
        {     
            \load_theme_textdomain( self::getTextDomain(), $this->getThemeDir( '/languages' ) );
            \add_action( 'after_setup_theme', array( &$this, 'initThemesOptions') );	            
            \add_action( 'wp_enqueue_scripts', array( &$this, 'initScriptsAndStyles') );//Подключаем вывод основных стилей

//        add_action( 'wp_enqueue_scripts', array( &$this, 'load_script_links') );//Подключаем вывод основных скриптов
        // add_filter( 'locale', array( &$this, 'set_stella_locale' ) ); //Корректируем локализацию темы от плагина STELLA
        // add_filter( 'stella_lang_name', array( &$this, 'change_lang_names' ) );//Изменяем отображение языков у плагина STELLA
//        add_filter( 'image_size_names_choose', array( &$this, 'thumbnails_custom_sizes' ) );// Подключаем размер дополнительных миниатюр к админке

//        add_filter( 'show_post_views', array( &$this, 'post_views' ), 10 ,1 );
//        add_action( 'wp', array( &$this, 'post_views_count' ), 10 ,0 );
        \add_action( 'wp', array( &$this, 'clearWordpressHead' ), 10 ,0 );
        }
    }
}


?>