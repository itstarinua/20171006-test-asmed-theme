<?php use DStaroselskiy\Wordpress\Themes\ThemeController;
get_header(); 
$current_term = get_queried_object();
?><section id="blog-top-section" class="blog-top-section">
    <img src="<?php echo ThemeController::getUrl('images/icon_blog.jpg');?>" title="<?php ThemeController::the_header_name()?>" alt="<?php ThemeController::the_header_name()?>">
    <?php ThemeController::the_header_name( '<h1 class="page-title">', '</h1>' ); ?>
    <?php the_archive_description( '<div class="taxonomy-description">', '</div>' );?>
</section>
<main id="main" class="site-main blog-section have-right-sidebar dms-container" role="main">
    <?php  ob_start(); ?>
        <h5 class="widgettitle"><?php _e('Categories ', ThemeController::getTextDomain() );?></h5>
        <?php $categorys = get_terms( 'category', array(
                'taxonomy'      => 'post',
                'orderby'       => 'name', 
                'order'         => 'ASC',
                'hide_empty'    => false, 
                'fields'        => 'all', 
                'hierarchical'  => false, 
                'get'           => 'all',
                'pad_counts'    => false, 
                'update_term_meta_cache' => true,
        ) ); ?>
        <ul class="terms-lists">
            <?php foreach ($categorys as $category) { 
                $term_select = "";
		if( isset($current_term->term_id) && ( $current_term->term_id == $category->term_id ) ) $term_select = "term-active"; ?>
                <li class="terms term_id-<?php echo  $category->term_id;?> term_slug-<?php echo  $category->slug;?>  <?php echo $term_select ;?>">
                    <a href="<?php echo get_term_link( $category->term_id, 'category' );?>" title="<?php echo $category->name;?>">
                        <?php echo $category->name;?>
                    </a>
                </li>
            <?php } ?>
        </ul>	
    <?php global $category_widget;
    $category_widget = ob_get_clean();?>
    <!-- noindex --><ul class="categories-mobile visible-xs visible-sm">
                        <li class="cat-info widget">
                            <?php echo $category_widget; ?>
                        </li>    
    <!-- /noindex --></ul>
    <section id="primary" class="blog-container content-area">
        <?php if ( \have_posts() ) :
                while ( have_posts() ) :
                    the_post();
                    ThemeController::get_template_part( 'blog', \get_post_format() );
                endwhile;

                // Previous/next page navigation.
                the_posts_pagination( array(
                        'prev_text'          => __( 'Previous page', ThemeController::getTextDomain() ),
                        'next_text'          => __( 'Next page', ThemeController::getTextDomain() ),
                        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', ThemeController::getTextDomain() ) . ' </span>',
                ) );

        // If no content, include the "No posts found" template.
        else :
            ThemeController::get_template_part( 'blog', 'none' );
        endif; ?>
    </section>
    <?php get_sidebar(); ?>
</main>
<script>
    jQuery(document).ready(function(){
        jQuery('.menu-item a[href="<?php echo home_url( get_option('category_base', 'category') );?>/"]').each(function(){
            $li = jQuery(this).parent();
            if( !$li.hasClass('current-menu-item') ) $li.addClass('current-menu-item');
            if( !$li.hasClass('current_page_item') ) $li.addClass('current_page_item');
            if( !$li.hasClass('current_page_parent') ) $li.addClass('current_page_parent');
        });
    });
</script>
<?php get_footer(); ?>
